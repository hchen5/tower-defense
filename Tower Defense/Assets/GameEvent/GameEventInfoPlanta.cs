using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "GameEventInfoPlanta", menuName = "GameEvent/GameEvent - InfoPlanta")]
public class GameEventInfoPlanta : GameEvent<InfoPlanta> { }