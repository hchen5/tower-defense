using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Enemy Info", menuName = "Scriptables/Enemy Info")]
public class EnemyInfo : ScriptableObject
{
    public float z_Speed = 0f;
    public Vector3 scale;
    public string nom;
    public int z_VidaMax;
    public int z_VidaActual;
    public float AtackTime;
}
