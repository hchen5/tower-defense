using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "InfoPlanta", menuName = "ScriptableObjects/Info Planta")]
public class InfoPlanta : ScriptableObject
{
    public string Nombre;
    public int HP_Max;
    public float AttackingSpeed;
    public int CosteSoles;
    public Sprite[] Sprites;


}
