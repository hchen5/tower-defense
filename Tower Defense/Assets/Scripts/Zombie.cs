using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Zombie : MonoBehaviour
{
    //[SerializeField]
    //private ElMeuScriptable z_vida;
   
   //[SerializeField]
    //private GameEvent z_vidaEvent;
    private Animator m_Animator;

    private float m_Speed=1f;
    private Rigidbody2D Zombie_movimiento;
    
    private EnemyInfo m_EnemyInfo;

    private string nom;
    int vida;
    
    private bool Atacar=false;
    //private PeaShooter p_PeaS;

    void Awake()
	{
        m_Animator = GetComponent<Animator>();
        Zombie_movimiento = GetComponent<Rigidbody2D>();
    }
    

    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.tag == "proyectilPS")
            ReceiveDamage();
        else if (collision.gameObject.tag == "planta")
        {
            m_Animator.SetBool("comer", true);
            Zombie_movimiento.velocity = new Vector3(0, 0, 0);
            StartCoroutine(Atack(collision));
        }

    }
    

    IEnumerator Atack(Collider2D collision) 
    {
        collision.gameObject.GetComponent<PlantBehaviour>().ReceiveDamage();
        yield return new WaitForSeconds(m_EnemyInfo.AtackTime);
        StartCoroutine(Atack(collision));
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
       
         if (collision.gameObject.tag == "planta")
        {
            Zombie_movimiento.velocity = new Vector3(-m_Speed, Zombie_movimiento.velocity.y, 0);
            m_Animator.SetBool("comer", false);
        }
    }
   
    private void ToDie()
    {
        GetComponent<Pooleable>().ReturnToPool();
    }
    private void ReceiveDamage()
    {
        vida--;
        if (vida <= 0)
            ToDie();
    }
    public void LoadInfo(EnemyInfo informacio)
    {
        m_EnemyInfo = informacio;
        nom = informacio.nom;
        Zombie_movimiento.transform.localScale = informacio.scale;
        Zombie_movimiento.velocity = new Vector3(-informacio.z_Speed, Zombie_movimiento.velocity.y, 0);
        vida = informacio.z_VidaMax;
    }
}
