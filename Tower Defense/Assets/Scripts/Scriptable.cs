using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Scriptable : ScriptableObject
{
    public int valorActual;
    public int valorMax;
}
