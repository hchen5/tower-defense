using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "TilePlantable", menuName = "Scripts/TilePlantable")]
/// <summary>
/// A tile that stores whether its passable or not.
/// </summary>
public class TilePlantable : Tile
{
    // I'm using a serialized private bool here so I don't have to bother writing a custom inspector for the property
    [SerializeField]
    private bool plantable;

    /// <summary>
    /// Gets if an entity can walk through the tile
    /// </summary>
    public bool TilePlantableBool
    {
        get
        {
            return plantable;
        }
    }
}
