using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GUIText : MonoBehaviour
{
    [SerializeField]
    private ElMeuScriptable m_ElementAMostrar;
    public void ActualitzarValors()
    {
        GetComponent<TextMeshProUGUI>().text =
            m_ElementAMostrar.ValorActual+"";
    }
}

