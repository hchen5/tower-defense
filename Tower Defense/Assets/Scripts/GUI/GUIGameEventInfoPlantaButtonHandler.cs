using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIGameEventInfoPlantaButtonHandler : MonoBehaviour
{
    [Tooltip("Par�metro que enviar� el evento")]
    [SerializeField]
    InfoPlanta m_InfoPlanta;

    [Tooltip("Evento que se enviar�")]
    [SerializeField]
    GameEventInfoPlanta m_GameEvent;

    public void Raise()
    {
        m_GameEvent.Raise(m_InfoPlanta);
    }
}
