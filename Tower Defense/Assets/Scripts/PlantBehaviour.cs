using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantBehaviour : MonoBehaviour
{
    string name;
    int CurrentHP;
    int CosteSoles;
    float AttackingSpeed;
    Sprite[] Sprites;
    InfoPlanta m_info1;

    private Pool m_pool;

    private Rigidbody2D m_rigidBody;
    private SpriteRenderer m_spriteRenderer;

    public Coroutine shootingCoroutine;
    private void Awake()
    {
        m_rigidBody = GetComponent<Rigidbody2D>();
        m_spriteRenderer = GetComponent<SpriteRenderer>();
    }


    public void LoadInfo(InfoPlanta info)
    {
        this.m_info1 = info;
        this.name = info.name;
        this.CurrentHP = info.HP_Max;
        this.Sprites = info.Sprites;
        this.AttackingSpeed = info.AttackingSpeed;
        this.m_spriteRenderer.sprite = info.Sprites[0];
        this.CosteSoles = info.CosteSoles;

        if (info.Nombre == "PeaShooter")
        {
            m_pool = GameManager.Instance.DisparoPool;
            m_spriteRenderer.sprite = Sprites[0];
            StartCoroutine(ShootCoroutine());

        }
        else if (info.Nombre == "SunFlower")
        {
            m_pool = GameManager.Instance.Soles;
            m_spriteRenderer.sprite = Sprites[0];
            StartCoroutine(ShootCoroutine());
        }
        else if (info.Nombre == "WallNut")
        {
            m_spriteRenderer.sprite = Sprites[0];
        }
       

    }

    IEnumerator ShootCoroutine()
    {
        yield return new WaitForSeconds(this.AttackingSpeed);
        while (true)
        {
            Attack();
            yield return new WaitForSeconds(this.AttackingSpeed);
        }
    }



    public void ReceiveDamage()
    {
        CurrentHP--;
        if (name == "WallNut")
        {
            if (CurrentHP < m_info1.HP_Max / 3)
                m_spriteRenderer.sprite = Sprites[2];
            else if (CurrentHP < m_info1.HP_Max / 2)
                m_spriteRenderer.sprite = Sprites[1];
        }
        if (CurrentHP <= 0)
            ToDie();
    }
    void ToDie()
    {
        StopAllCoroutines();
        //Vector3Int v = (Vector3Int) Camera.main.ScreenToWorldPoint(GetComponent<Rigidbody2D>().position);
        // GameManager.Instance.m_Plantades.Remove(v);
        GetComponent<Pooleable>().ReturnToPool();
    }

    void Attack()
    {
        GameObject proyectil = m_pool.GetElement();
        if (proyectil)
            proyectil.GetComponent<Rigidbody2D>().transform.position = new Vector2(this.transform.position.x, this.transform.position.y+0.5f);

        if (name == "PeaShooter")
            proyectil.GetComponent<Rigidbody2D>().velocity = new Vector2(3f,0);




    }

}
