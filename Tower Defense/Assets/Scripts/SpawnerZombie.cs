using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerZombie : MonoBehaviour
{
    //[SerializeField]
    //private GameObject[] m_ElementASpawnejar;
    [SerializeField]
    private float m_SpawnRate = 3f;
    //private float m_SpawnRateDelta = 3f;
    [SerializeField]
    private Transform[] m_SpawnPoints;

    [SerializeField]
    private EnemyInfo[] enemyInfos;
    
    //private Pool z_pool;

    private void Awake()
    {
       
    }
    void Start()
    {
        //m_SpawnRateDelta = m_SpawnRate;
        //z_pool = GameManager.Instance.ZombiePool;
        StartCoroutine(SpawnCoroutine());
       
    }
    
    IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            GameObject Spawn = GameManager.Instance.ZombiePool.GetElement();
            if (Spawn)
            {
             
                Spawn.transform.position = m_SpawnPoints[Random.Range(0, m_SpawnPoints.Length)].position;
                Spawn.GetComponent<Zombie>().LoadInfo(enemyInfos[Random.Range(0,enemyInfos.Length)]);
                yield return new WaitForSeconds(m_SpawnRate);
            }
            
            
        }
    }
}
