using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public class GameManager : MonoBehaviour
{
    private static GameManager m_Instance;
    public static GameManager Instance
    {
        get { return m_Instance; }
    }

    [SerializeField]
    private ElMeuScriptable m_Sol;

    public List<Vector3Int> m_Plantades = new List<Vector3Int>();


    [SerializeField]
    private Tile[] m_PlantableTiles;

    [SerializeField]
    Pool m_ZombiePool;
    public Pool ZombiePool => m_ZombiePool;

    [SerializeField]
    Pool m_DisparoPool;
    public Pool DisparoPool => m_DisparoPool;

    [SerializeField]
    Pool m_Soles;
    public Pool Soles => m_Soles;

    [SerializeField]
    Pool m_PlantasPool;
    public Pool PlantasPool => m_PlantasPool;

    [SerializeField]
    private Tilemap m_Tilemap;
    [SerializeField]
    private GameObject m_Planta;

    [SerializeField]
    private GameEvent ClickSoles;

    private bool m_SePlanta;
    private GameObject m_PlantaEdicion;
    private InfoPlanta m_InfoPlantaEdicion;
    Vector3Int m_Origin;
    private void Awake()
    {
        if (m_Instance == null)
            m_Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
        m_SePlanta = false;
    }

    private void Update()
    {
        if (m_SePlanta)
        {
            Vector3 pointerPosition = m_Tilemap.GetCellCenterWorld(m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition)));
            pointerPosition.z = 0;
            pointerPosition.y -= 0.5f;
            m_PlantaEdicion.transform.position = pointerPosition;
            if (Input.GetMouseButtonDown(0))
            {
                m_Origin = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
                print(m_Origin);
                if (IsPlantable(m_Origin) && !EstaPlantado(m_Origin))
                {
                    m_SePlanta = false;
                    Destroy(m_PlantaEdicion);

                    GameObject PlantaNueva = GameManager.Instance.PlantasPool.GetElement();
                    if (PlantaNueva)
                    {
                        PlantaNueva.transform.position = pointerPosition;
                        PlantaNueva.GetComponent<PlantBehaviour>().LoadInfo(m_InfoPlantaEdicion);
                    }
                    //Restar Soles con el coste del infoplanta
                    RestarSoles(m_InfoPlantaEdicion.CosteSoles);
                }
            }
        }
    }

    public void OnclickSpawnPlanta(InfoPlanta infoPlanta)
    {
        if (m_Sol.ValorActual >= infoPlanta.CosteSoles)
        {
            
            if (m_PlantaEdicion)
            {
                Destroy(m_PlantaEdicion);
                m_SePlanta = false;
            }
            m_SePlanta = true;
            m_InfoPlantaEdicion = infoPlanta;
            if (!m_PlantaEdicion)
            {
                
                m_PlantaEdicion = Instantiate(m_Planta);
                m_PlantaEdicion.GetComponent<SpriteRenderer>().sprite = m_InfoPlantaEdicion.Sprites[0];
                //TODO poner el sprite correspondiente a la planta temporal
            }

        }

    }


    public bool IsPlantable(Vector3Int target)
    {
        Tile tileClicat = m_Tilemap.GetTile<Tile>(target);
        return m_PlantableTiles.Contains<Tile>(tileClicat);
    }
    public bool EstaPlantado(Vector3Int target)
    {
        if (m_Plantades.Contains(target))
        {
            return true;
        }
        else
        {
            m_Plantades.Add(target);
            return false;
        }

    }
    public void SumoSoles()
    {
        m_Sol.ValorActual += 25;
        ClickSoles.Raise();
    }
    public void RestarSoles(int Coste)
    {
        m_Sol.ValorActual -= Coste;
        ClickSoles.Raise();
    }

    public void GameOver()
    {
        SceneManager.LoadScene("GameOver");
    }

    public void Restart()
    {
        SceneManager.LoadScene("Lvl1");
    }

    public void GameInici() 
    {
        SceneManager.LoadScene("EscenaInicial");
    }
    public void Desplantar(GameObject Planta)
    {
        int x = (int)Planta.GetComponent<Rigidbody>().position.x;
        int y = (int)Planta.GetComponent<Rigidbody>().position.y;
        int z = (int)Planta.GetComponent<Rigidbody>().position.z;
        Vector3Int Desplantado = new Vector3Int(x, y, z);

        if (m_Plantades.Contains(Desplantado))
        {
            m_Plantades.Remove(Desplantado);
        }
    }
}
