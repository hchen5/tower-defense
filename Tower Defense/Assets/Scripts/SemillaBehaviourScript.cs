using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SemillaBehaviourScript : MonoBehaviour
{
    [SerializeField]
    private float m_Velocity = 3f;
    [SerializeField]
    private float Damage = 20f;

    private void Awake()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(m_Velocity, 0);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "elMuro" || collision.tag=="zombie")
            GetComponent<Pooleable>().ReturnToPool();
    }


}
