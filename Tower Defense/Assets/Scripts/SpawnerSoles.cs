using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerSoles : MonoBehaviour
{
    [SerializeField]
    private float m_SpawnRate = 5f;
    //  private float m_SpawnRateDelta = 3f;

    [SerializeField]
    private Transform[] m_SpawnPoints;

    void Start()
    {
        //m_SpawnRateDelta = m_SpawnRate;

        StartCoroutine(SpawnCoroutine());
    }

    /*
    // Update is called once per frame
    void Update()
    {
        m_SpawnRateDelta -= Time.deltaTime;
        if(m_SpawnRateDelta <= 0f)
        {
            m_SpawnRateDelta += m_SpawnRate;
            GameObject spawned = Instantiate(m_ElementASpawnejar);
            spawned.transform.position = transform.position;
        }
    }
    */

    IEnumerator SpawnCoroutine()
    {
        while (true)
        {
            GameObject spawned = GameManager.Instance.Soles.GetElement();
            spawned.transform.position = m_SpawnPoints[Random.Range(0, m_SpawnPoints.Length)].position;
            yield return new WaitForSeconds(m_SpawnRate);
        }
    }
}
