using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[RequireComponent(typeof(Plantable))]
public class DemoPlantable : MonoBehaviour
{
    [SerializeField]
    Tilemap m_Tilemap;

    Plantable m_Plantable;
    Vector3Int m_Origin;

    private void Awake()
    {
        m_Plantable = GetComponent<Plantable>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            m_Origin = m_Tilemap.WorldToCell(Camera.main.ScreenToWorldPoint(Input.mousePosition));
            print(m_Origin);
            if (m_Plantable.IsPlantable(m_Origin))
            {
                print("ES PLANTABLE");
            }
            else
            {
                print("NO ES PLANTABLE");
            }

        }
        
        
    }


}
