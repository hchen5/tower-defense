using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Plantable : MonoBehaviour
{
    [SerializeField]
    private Tilemap m_tilemap;

    [SerializeField]
    private Tile[] m_PlantableTiles;

    //returns true if target is inside plantabletiles array
    public bool IsPlantable(Vector3Int target)
    {
        Tile tileClicat = m_tilemap.GetTile<Tile>(target);
        return m_PlantableTiles.Contains<Tile>(tileClicat);
    }

}
