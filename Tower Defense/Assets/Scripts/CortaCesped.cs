using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CortaCesped : MonoBehaviour
{
    private float m_Speed=5f;
    private Rigidbody2D C_Cesped;
    
    void Awake()
    {
        print("CortaCesped creat");
        C_Cesped=GetComponent<Rigidbody2D>();
    }
    
    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
        print("CortaCesped Destroyed");
    }

   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "zombie")
        {
            C_Cesped.velocity = new Vector3(m_Speed, C_Cesped.velocity.y, 0);
            
            collision.GetComponent<Pooleable>().ReturnToPool();
        }
    }

}
